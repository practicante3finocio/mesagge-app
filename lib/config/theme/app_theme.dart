import 'package:flutter/material.dart';

const List<Color> _colorThemes = [
  Colors.blue,
  Colors.black,
  Colors.purple,
  Colors.red,
  Colors.green,
  Colors.teal,
  Colors.pink
];

class AppTheme {
  final int selectColor;
  AppTheme({required this.selectColor})
      : assert(selectColor >= 0 && selectColor < _colorThemes.length,
            'SelectColor must be betwen 0 and ${_colorThemes.length}');
  ThemeData theme() {
    return ThemeData(
        useMaterial3: true, 
        colorSchemeSeed: _colorThemes[selectColor],
        );
  }
}
