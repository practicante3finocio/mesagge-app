import 'package:flutter/material.dart';

class MassageBox extends StatelessWidget {
  final ValueChanged<String> onValue;

  const MassageBox({
    super.key,
    required this.onValue
    });

  @override
  Widget build(BuildContext context) {
    final textController = TextEditingController();
    final focusNode = FocusNode();
    ///final colors = Theme.of(context).colorScheme;
    final outlineInputBorder = UnderlineInputBorder(
      borderSide: const BorderSide(color: Colors.transparent),
      borderRadius: BorderRadius.circular(40),
    );
    final inputDecoration = InputDecoration(
      hintText: 'End your message with a "?"',
        enabledBorder: outlineInputBorder,
        focusedBorder: outlineInputBorder,
        filled: true,
        suffixIcon: IconButton(
          icon: const Icon(Icons.send_outlined),
          onPressed: () {
            final textValue = textController.value.text;
            textController.clear();
            onValue(textValue);
          },
        )
      );


    return TextFormField(
      focusNode: focusNode,
      onTapOutside: (event) => focusNode.unfocus(),
      controller : textController,
      decoration: inputDecoration,
      onFieldSubmitted: (value) {
        focusNode.requestFocus();
        textController.clear();
        onValue(value);
      },
    );
  }
}